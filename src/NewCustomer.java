import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.DropMode;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

@SuppressWarnings({ "unused", "serial" })
public class NewCustomer extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField customerName;
	private JTextField customerPhoneNo;
	private JTextField customerColorCode1;
	private JTextField customerColorCode2;
	private JTextField customerColorCode3;
	private JTextField customerAddress;
	
	String name = null; 
	String phoneNo = null;
	String address = null;
	String colorCode1 = null;
	String colorCode2 = null;
	String colorCode3 = null;
	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		try {
			NewCustomer dialog = new NewCustomer();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public NewCustomer() {

		setAlwaysOnTop(true);

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(NewCustomer.class.getResource("/icons/add_list.png")));
		
		InsertIntoDB insDB = new InsertIntoDB(); //To use the Insert(ID, varToInsert) function
		
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setTitle("Add new customer info");
		setResizable(false);
		getContentPane().setFont(new Font("Verdana", Font.PLAIN, 14));
		setBounds(100, 100, 387, 317);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		customerName = new JTextField(name);
		customerName.setFont(new Font("Verdana", Font.PLAIN, 14));
		customerName.setColumns(10);
		customerName.setBounds(128, 11, 233, 27);
		contentPanel.add(customerName);
		
		customerPhoneNo = new JTextField(phoneNo);
		customerPhoneNo.setFont(new Font("Verdana", Font.PLAIN, 14));
		customerPhoneNo.setColumns(10);
		customerPhoneNo.setBounds(128, 49, 233, 27);
		contentPanel.add(customerPhoneNo);
		
		customerAddress = new JTextField(address);
		customerAddress.setToolTipText("");
		customerAddress.setFont(new Font("Verdana", Font.PLAIN, 14));
		customerAddress.setColumns(10);
		customerAddress.setBounds(128, 87, 233, 27);
		contentPanel.add(customerAddress);
		
		customerColorCode1 = new JTextField(colorCode1);
		customerColorCode1.setToolTipText("If applicable");
		customerColorCode1.setFont(new Font("Verdana", Font.PLAIN, 14));
		customerColorCode1.setColumns(10);
		customerColorCode1.setBounds(128, 125, 233, 27);
		contentPanel.add(customerColorCode1);
		
		customerColorCode2 = new JTextField(colorCode2);
		customerColorCode2.setToolTipText("If applicable");
		customerColorCode2.setFont(new Font("Verdana", Font.PLAIN, 14));
		customerColorCode2.setColumns(10);
		customerColorCode2.setBounds(128, 163, 233, 27);
		contentPanel.add(customerColorCode2);
		
		customerColorCode3 = new JTextField(colorCode3);
		customerColorCode3.setToolTipText("If applicable");
		customerColorCode3.setFont(new Font("Verdana", Font.PLAIN, 14));
		customerColorCode3.setColumns(10);
		customerColorCode3.setBounds(128, 201, 233, 27);
		contentPanel.add(customerColorCode3);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblName.setBounds(10, 11, 88, 27);
		contentPanel.add(lblName);
		
		JLabel lblPhoneNo_1 = new JLabel("Phone No.:");
		lblPhoneNo_1.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblPhoneNo_1.setBounds(10, 49, 88, 27);
		contentPanel.add(lblPhoneNo_1);
		
		JLabel lblPhoneNo = new JLabel("Address:");
		lblPhoneNo.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblPhoneNo.setBounds(10, 87, 108, 27);
		contentPanel.add(lblPhoneNo);
		
		JLabel label_2 = new JLabel("Color Code 1:");
		label_2.setFont(new Font("Verdana", Font.PLAIN, 14));
		label_2.setBounds(10, 125, 108, 27);
		contentPanel.add(label_2);
		
		JLabel label_3 = new JLabel("Color Code 2:");
		label_3.setFont(new Font("Verdana", Font.PLAIN, 14));
		label_3.setBounds(10, 164, 108, 24);
		contentPanel.add(label_3);
		
		JLabel lblColorCode = new JLabel("Color Code 3:");
		lblColorCode.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblColorCode.setBounds(10, 202, 108, 24);
		contentPanel.add(lblColorCode);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						name = customerName.getText();
						phoneNo = customerPhoneNo.getText();
						address = customerAddress.getText();
						colorCode1 = customerColorCode1.getText();
						colorCode2 = customerColorCode2.getText();
						colorCode3 = customerColorCode3.getText();
						
						insDB.Insert(name, phoneNo, address, colorCode1, colorCode2, colorCode3);
						
						dispose();
						customerName.setText(""); //Clear text fields when Dialog closes (this and below 5 lines)
						customerPhoneNo.setText("");
						customerAddress.setText("");
						customerColorCode1.setText("");
						customerColorCode2.setText("");
						customerColorCode3.setText("");
						
						
					}
				});
				okButton.setFont(new Font("Verdana", Font.PLAIN, 14));
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						dispose();
						customerName.setText(""); //Clear text fields when Dialog closes (this and below 5 lines)
						customerPhoneNo.setText("");
						customerAddress.setText("");
						customerColorCode1.setText("");
						customerColorCode2.setText("");
						customerColorCode3.setText("");
					}
				});
				cancelButton.setFont(new Font("Verdana", Font.PLAIN, 14));
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
